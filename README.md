# Project: RedditAPI_Android

Android native client for reddit.com/top

- Load news with pagination (10 items per page)
- Support full sized thumb picture.
- App state preservation/restoration

## Technical solutions
* **DI** - Dagger 2
* **Design** - MVP with plugin support
* **Network** - Retrofit2
* **ImageLoader** - Picasso


# Screens
![picture](img/screen_news.png)
![picture](img/screen_thumb.png)
