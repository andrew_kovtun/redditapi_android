package com.uniplay.reddit.application;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.uniplay.reddit.dagger.component.ApplicationComponent;
import com.uniplay.reddit.dagger.component.DaggerApplicationComponent;
import com.uniplay.reddit.dagger.module.ApplicationModule;

public class RedditApplication extends Application {
    private static final String TAG = "{RedditApplication}";

    private ApplicationComponent applicationComponent;

    public static RedditApplication get(@NonNull Context context) {
        return (RedditApplication) context.getApplicationContext();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        initializeInjector();
    }

    private void initializeInjector() {
        this.applicationComponent = buildApplicationComponent();
        applicationComponent.inject(this);
    }

    protected ApplicationComponent buildApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this.getApplicationContext()))
                .build();
    }
}
