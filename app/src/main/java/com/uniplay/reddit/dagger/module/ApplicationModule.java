package com.uniplay.reddit.dagger.module;

import android.content.Context;
import android.util.Log;

import com.uniplay.common.util.NetworkUtils;
import com.uniplay.domain.plugin.Plugin;
import com.uniplay.domain.repository.DataStorage;
import com.uniplay.common.persister.ComponentPersister;
import com.uniplay.reddit.dagger.scope.ApplicationScope;
import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.plugin.RedditPlugin;
import com.uniplay.reddit.screens.news.interactor.api.RedditAPI;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApplicationModule {
    private static final String TAG = "{ApplicationModule}";
    private Context appContext;

    public ApplicationModule(Context appContext) {
        this.appContext = appContext;
    }

    @Provides
    @ApplicationScope
    Plugin providePlugin() {
        return new RedditPlugin();
    }

    @Provides
    @ApplicationScope
    Retrofit provideNetworkService(Plugin plugin) {
        Log.d(TAG, String.format("initializeServices: plugin = [%s]", plugin.getName()));
        return new Retrofit.Builder()
                .baseUrl(plugin.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @ApplicationScope
    RedditAPI provideNetworkApi(Retrofit retrofit) {
        return retrofit.create(RedditAPI.class);
    }

    @Provides
    @ApplicationScope
    DataStorage<NewsEntity> provideCache() {
        return new DataStorage<>();
    }

    @Provides
    @ApplicationScope
    Context provideContext() {
        return appContext;
    }

    @Provides
    @ApplicationScope
    NetworkUtils provideNetworkUtils() {
        return new NetworkUtils();
    }
}
