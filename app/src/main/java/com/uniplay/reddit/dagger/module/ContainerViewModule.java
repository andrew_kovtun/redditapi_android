package com.uniplay.reddit.dagger.module;

import com.uniplay.reddit.dagger.scope.ActivityScope;
import com.uniplay.reddit.screens.container.presenter.ContainerScreenPresenter;
import com.uniplay.reddit.screens.container.presenter.DefaultContainerScreenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ContainerViewModule {

    @Provides
    @ActivityScope
    ContainerScreenPresenter providePresenter() {
        return new DefaultContainerScreenPresenter();
    }
}
