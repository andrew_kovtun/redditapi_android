package com.uniplay.reddit.dagger.component.api;

import android.content.Context;

import com.uniplay.common.util.NetworkUtils;
import com.uniplay.domain.repository.DataStorage;
import com.uniplay.common.persister.ComponentPersister;
import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.screens.news.interactor.api.RedditAPI;

import retrofit2.Retrofit;

public interface ApplicationComponentAPI {

    Context provideApplicationContext();

    DataStorage<NewsEntity> provideDataStorage();

    Retrofit getNetworkService();

    RedditAPI provideRedditApi();

    NetworkUtils provideNetworkUtils();
}
