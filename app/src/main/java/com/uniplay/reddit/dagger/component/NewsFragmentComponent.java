package com.uniplay.reddit.dagger.component;

import com.uniplay.reddit.dagger.module.NewsScreenModule;
import com.uniplay.reddit.dagger.scope.ActivityScope;
import com.uniplay.reddit.screens.news.ui.NewsScreenFragment;

import dagger.Component;

@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = {NewsScreenModule.class})
public interface NewsFragmentComponent {

    void inject(NewsScreenFragment newsScreenFragment);
}
