package com.uniplay.reddit.dagger.component;

import com.uniplay.reddit.dagger.module.ThumbScreenModule;
import com.uniplay.reddit.dagger.scope.ActivityScope;
import com.uniplay.reddit.screens.thumb.ui.ThumbScreenFragment;

import dagger.Component;

@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = ThumbScreenModule.class)
public interface ThumbFragmentComponent {
    void inject(ThumbScreenFragment thumbFragment);
}
