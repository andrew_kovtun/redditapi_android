package com.uniplay.reddit.dagger.module;

import com.uniplay.reddit.screens.thumb.presenter.BaseThumbScreenPresenter;
import com.uniplay.reddit.screens.thumb.presenter.ThumbScreenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ThumbScreenModule {

    @Provides
    ThumbScreenPresenter providePresenter() {
        return new BaseThumbScreenPresenter();
    }
}
