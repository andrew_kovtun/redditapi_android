package com.uniplay.reddit.dagger.module;

import android.content.Context;

import com.uniplay.domain.repository.DataStorage;
import com.uniplay.reddit.R;
import com.uniplay.reddit.dagger.scope.ActivityScope;
import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.manager.PageManager;
import com.uniplay.reddit.screens.news.adapter.RecyclerAdapter;
import com.uniplay.reddit.screens.news.interactor.api.RedditAPI;
import com.uniplay.reddit.screens.news.interactor.NetworkNewsUseCase;
import com.uniplay.reddit.screens.news.interactor.NewsUseCase;
import com.uniplay.reddit.screens.news.presenter.BaseItemsPresenter;
import com.uniplay.reddit.screens.news.presenter.BaseNewsScreenPresenter;
import com.uniplay.reddit.screens.news.presenter.ListItemsPresenter;
import com.uniplay.reddit.screens.news.presenter.NewsScreenPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsScreenModule {

    @Provides
    @ActivityScope
    NewsScreenPresenter providePresenter(BaseNewsScreenPresenter mainPresenter) {
        return mainPresenter;
    }

    @Provides
    @ActivityScope
    RecyclerAdapter provideListAdapter(ListItemsPresenter presenter) {
        return new RecyclerAdapter(presenter);
    }

    @Provides
    @ActivityScope
    ListItemsPresenter provideItemsPresenter() {
        return new BaseItemsPresenter();
    }

    @Provides
    @ActivityScope
    PageManager providePageManager(Context ctx) {
        int limit = ctx.getResources().getInteger(R.integer.page_items_count);
        return new PageManager(limit);
    }

    @Provides
    @ActivityScope
    NewsUseCase provideNetworkDataProvider(RedditAPI api, DataStorage<NewsEntity> dataStorage, PageManager pageManager) {
        return new NetworkNewsUseCase(api, dataStorage, pageManager);
    }
}
