package com.uniplay.reddit.dagger.component;

import com.uniplay.reddit.application.RedditApplication;
import com.uniplay.reddit.dagger.component.api.ApplicationComponentAPI;
import com.uniplay.reddit.dagger.module.ApplicationModule;
import com.uniplay.reddit.dagger.scope.ApplicationScope;

import dagger.Component;

@ApplicationScope
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent extends ApplicationComponentAPI {

    void inject(RedditApplication application);
}