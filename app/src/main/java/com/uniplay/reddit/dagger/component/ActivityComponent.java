package com.uniplay.reddit.dagger.component;

import com.uniplay.reddit.dagger.module.ContainerViewModule;
import com.uniplay.reddit.dagger.scope.ActivityScope;
import com.uniplay.reddit.screens.container.ui.ContainerActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {ContainerViewModule.class})
public interface ActivityComponent {
    void inject(ContainerActivity mainActivity);
}
