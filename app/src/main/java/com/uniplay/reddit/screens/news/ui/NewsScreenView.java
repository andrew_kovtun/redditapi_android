package com.uniplay.reddit.screens.news.ui;

import android.support.annotation.StringRes;

public interface NewsScreenView<T> {

    void showProgress();

    void hideProgress();

    void showError(@StringRes int resId);

    void showNoDataLayer(@StringRes int resId);

    void hideNoDataLayer();
}
