package com.uniplay.reddit.screens.container.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.uniplay.common.ui.BaseActivity;
import com.uniplay.common.ui.Layer;
import com.uniplay.reddit.R;
import com.uniplay.reddit.dagger.component.ActivityComponent;
import com.uniplay.reddit.dagger.component.DaggerActivityComponent;
import com.uniplay.reddit.dagger.module.ContainerViewModule;
import com.uniplay.reddit.screens.container.presenter.ContainerScreenPresenter;

import javax.inject.Inject;

public class ContainerActivity extends BaseActivity<ActivityComponent> implements ContainerScreenView {
    private static final String TAG = "{ContainerActivity}";

    private final RouteReceiver routeReceiver = new RouteReceiver();

    @Inject
    ContainerScreenPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, String.format("onCreate: savedInstanceState=[%s]", savedInstanceState));
        setContentView(R.layout.main_screen_activity);
        presenter.onAttachView(savedInstanceState, this);
    }

    @Override
    protected ActivityComponent buildComponent() {
        return DaggerActivityComponent.builder()
                .containerViewModule(new ContainerViewModule())
                .build();
    }

    @Override
    protected void initializeInjector(ActivityComponent component) {
        component.inject(this);
    }

    @Override
    protected Class getComponentClass() {
        return ActivityComponent.class;
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
        getBaseContext().registerReceiver(routeReceiver, presenter.getRoutesIntentFilter());
    }

    @Override
    public void showLayer(@NonNull Layer layer) {
        Log.d(TAG, String.format("showLayer: layer=[%s]", layer));
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, layer.getContentFragment())
                .commit();
    }

    @Override
    public void addLayer(@NonNull Layer layer) {
        Log.d(TAG, String.format("addLayer: layer=[%s]", layer));
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, layer.getContentFragment())
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
        getBaseContext().unregisterReceiver(routeReceiver);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed()");
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStack();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
        presenter.onDetachView();
    }

    private class RouteReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG + "#RouteReceiver", "onDestroy()");
            presenter.onRouteAction(intent);
        }
    }
}
