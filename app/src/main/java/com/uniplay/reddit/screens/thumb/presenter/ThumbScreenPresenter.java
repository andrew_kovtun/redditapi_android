package com.uniplay.reddit.screens.thumb.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.uniplay.common.ui.Presenter;
import com.uniplay.reddit.screens.thumb.ui.ThumbScreenView;

public interface ThumbScreenPresenter extends Presenter<ThumbScreenView> {

    void onCreate(@Nullable Bundle arguments);

    void onLoadImageSuccess();

    void onLoadImageFailure();
}
