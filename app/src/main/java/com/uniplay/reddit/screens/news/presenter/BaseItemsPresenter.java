package com.uniplay.reddit.screens.news.presenter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Patterns;

import com.uniplay.reddit.R;
import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.domain.intent.NewsLocalIntent;
import com.uniplay.reddit.screens.news.adapter.RecyclerAdapter;
import com.uniplay.reddit.screens.news.ui.NewsViewHolder;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class BaseItemsPresenter implements ListItemsPresenter<NewsEntity, NewsViewHolder> {
    private static final String TAG = "{BaseItemsPresenter}";

    private List<NewsEntity> data = new LinkedList<>();

    private RecyclerAdapter adapter;

    @Override
    public void bindAdapter(RecyclerAdapter recyclerAdapter) {
        this.adapter = recyclerAdapter;
    }

    @Override
    public void onBindViewHolder(NewsViewHolder vh, int position) {
        NewsEntity newsItem = data.get(position);
        if (newsItem == null) {
            Log.e(TAG, "onBindViewHolder<ITEM IS NULL>");
            return;
        }
        vh.setTitle(newsItem.getTitle());
        vh.setAuthor(newsItem.getAuthor());
        vh.setCommentNumbers(newsItem.getCommentNumbers());
        vh.setDate(getUserFriendlyDate(newsItem.getCreatedInSec()));
        String thumbUriString = newsItem.getThumbnail();
        if (isThumbValid(thumbUriString)) {
            vh.setThumb(Uri.parse(thumbUriString));
        } else {
            vh.setThumb(newsItem.getDefaultDrawable());
        }
    }

    private boolean isThumbValid(String thumbUriString) {
        return !TextUtils.isEmpty(thumbUriString) && Patterns.WEB_URL.matcher(thumbUriString).matches();
    }

    private String getUserFriendlyDate(long timeInSec) {
        long now = System.currentTimeMillis();
        CharSequence ago = DateUtils.getRelativeTimeSpanString(timeInSec * 1000, now, DateUtils.MINUTE_IN_MILLIS);
        return ago.toString();
    }

    @Override
    public void onItemClick(NewsViewHolder viewHolder, int position) {
        Log.d(TAG, String.format("onItemClick: position=[%s]", position));
        NewsEntity newsItem = data.get(position);
        if (!isThumbValid(newsItem.getThumbnail())) {
            Log.e(TAG, "onItemClick<THUMB LINK IS NOT VALID>");
            viewHolder.showToast(R.string.thumb_load_error);
            return;
        }
        Context ctx = viewHolder.itemView.getContext();
        ctx.sendBroadcast(NewsLocalIntent.buildThumbScreenRouteIntent(newsItem));
    }

    @Override
    public int getItemViewType(int position) {
        return (position == data.size() - 1) ? RecyclerAdapter.LOADING_ITEM : RecyclerAdapter.CONTENT_ITEM;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void addItems(@NonNull Collection<NewsEntity> data) {
        Log.d(TAG, String.format("addItems: data=[%s]", data));
        this.data.addAll(data);
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void setItems(@NonNull Collection<NewsEntity> data) {
        Log.d(TAG, String.format("setItems: data=[%s]", data));
        this.data.clear();
        this.data.addAll(data);
        this.adapter.notifyDataSetChanged();
    }
}
