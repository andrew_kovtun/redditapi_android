package com.uniplay.reddit.screens.container.presenter;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.uniplay.common.lifecycle.LifecycleBehaviour;
import com.uniplay.common.ui.Presenter;
import com.uniplay.reddit.screens.container.ui.ContainerScreenView;

public interface ContainerScreenPresenter extends Presenter<ContainerScreenView> {

    void onAttachView(@Nullable Bundle savedInstanceState, @NonNull ContainerScreenView view);

    void onRouteAction(@Nullable Intent routeIntent);

    IntentFilter getRoutesIntentFilter();
}
