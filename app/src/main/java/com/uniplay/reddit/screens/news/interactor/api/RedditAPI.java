package com.uniplay.reddit.screens.news.interactor.api;

import com.uniplay.reddit.domain.entity.NewsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RedditAPI {

    @GET("top.json")
    Call<NewsResponse> getTop(@Query("limit") int limit, @Query("after") String after);
}
