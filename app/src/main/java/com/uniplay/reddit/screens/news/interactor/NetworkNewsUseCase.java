package com.uniplay.reddit.screens.news.interactor;

import android.support.annotation.NonNull;
import android.util.Log;

import com.uniplay.domain.repository.DataStorage;
import com.uniplay.domain.service.LoaderCallback;
import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.domain.entity.NewsResponse;
import com.uniplay.reddit.manager.PageManager;
import com.uniplay.reddit.screens.news.interactor.api.RedditAPI;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkNewsUseCase implements NewsUseCase {
    private final String TAG = "{NetworkNewsUseCase}";
    private final RedditAPI api;
    private final DataStorage<NewsEntity> dataStorage;
    private final PageManager pageManager;

    @Inject
    public NetworkNewsUseCase(RedditAPI api, DataStorage<NewsEntity> dataStorage, PageManager pageManager) {
        this.api = api;
        this.dataStorage = dataStorage;
        this.pageManager = pageManager;
    }

    @Override
    public void loadItems(@NonNull final LoaderCallback<List<NewsEntity>> loaderCallback) {
        Log.d(TAG, String.format("loadItems: limit=[%s]; after=[%s]", pageManager.getLimit(), pageManager.getAfter()));
        load(loaderCallback, pageManager.getLimit(), pageManager.getAfter());
    }

    @Override
    public void loadItems(@NonNull LoaderCallback<List<NewsEntity>> loaderCallback, String after) {
        Log.d(TAG, String.format("loadItems: limit=[%s]; after=[%s]", pageManager.getLimit(), after));
        load(loaderCallback, pageManager.getLimit(), after);
    }

    private void load(@NonNull final LoaderCallback<List<NewsEntity>> loaderCallback, int limit, String after) {
        api.getTop(limit, after).enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                Log.d(TAG, String.format("onResponse: response=[%s]", response));
                if (response == null || response.body() == null) {
                    loaderCallback.onFailure(new Throwable("EMPTY RESPONSE"));
                    return;
                }
                dataStorage.put(response.body().getNewsEntities());
                pageManager.setAfter(response.body().getAfter());
                loaderCallback.onSuccess(response.body().getNewsEntities());
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                Log.e(TAG, String.format("onResponse: error=[%s]", t));
                loaderCallback.onFailure(t);
            }
        });
    }
}
