package com.uniplay.reddit.screens.news.presenter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.uniplay.reddit.screens.news.adapter.RecyclerAdapter;

import java.util.Collection;

public interface ListItemsPresenter<T, VH extends RecyclerView.ViewHolder> {

    void bindAdapter(RecyclerAdapter recyclerAdapter);

    void onBindViewHolder(VH viewHolder, int position);

    void onItemClick(VH viewHolder, int position);

    int getItemViewType(int position);

    int getItemCount();

    void addItems(@NonNull Collection<T> data);

    void setItems(@NonNull Collection<T> data);
}
