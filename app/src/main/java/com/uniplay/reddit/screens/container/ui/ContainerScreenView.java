package com.uniplay.reddit.screens.container.ui;

import android.support.annotation.NonNull;

import com.uniplay.common.ui.Layer;

public interface ContainerScreenView {

    void addLayer(@NonNull Layer layer);

    void showLayer(@NonNull Layer layer);
}
