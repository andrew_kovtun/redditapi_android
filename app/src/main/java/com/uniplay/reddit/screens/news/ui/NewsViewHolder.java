package com.uniplay.reddit.screens.news.ui;

import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.uniplay.common.util.CircleTransformation;
import com.uniplay.reddit.R;

public class NewsViewHolder extends RecyclerView.ViewHolder implements NewsViewItem {

    public NewsViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setTitle(@NonNull String title) {
        ((TextView) itemView.findViewById(R.id.title_txt)).setText(title);
    }

    @Override
    public void setAuthor(@NonNull String name) {
        ((TextView) itemView.findViewById(R.id.author_txt)).setText(name);
    }

    @Override
    public void setDate(@NonNull String dateUserFriendly) {
        ((TextView) itemView.findViewById(R.id.date_txt)).setText(dateUserFriendly);
    }

    @Override
    public void setThumb(@Nullable Uri uri) {
        Picasso.with(itemView.getContext())
                .load(uri)
                .transform(new CircleTransformation())
                .into((ImageView) itemView.findViewById(R.id.thumb_container));
    }

    @Override
    public void setThumb(@DrawableRes int defaultDrawable) {
        Picasso.with(itemView.getContext())
                .load(defaultDrawable)
                .placeholder(defaultDrawable)
                .transform(new CircleTransformation())
                .into((ImageView) itemView.findViewById(R.id.thumb_container));
    }

    @Override
    public void setCommentNumbers(int count) {
        ((TextView) itemView.findViewById(R.id.comment_nmbrs_txt)).setText(String.valueOf(count));
    }

    @Override
    public void showToast(@StringRes int text) {
        Toast.makeText(this.itemView.getContext(), text, Toast.LENGTH_SHORT).show();
    }
}
