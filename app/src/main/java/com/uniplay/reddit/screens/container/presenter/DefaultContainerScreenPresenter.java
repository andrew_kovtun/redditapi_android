package com.uniplay.reddit.screens.container.presenter;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.uniplay.common.ui.Layer;
import com.uniplay.reddit.domain.intent.NewsActions;
import com.uniplay.reddit.domain.intent.NewsLocalIntent;
import com.uniplay.reddit.screens.container.ui.ContainerScreenView;
import com.uniplay.reddit.screens.news.ui.NewsScreenFragment;
import com.uniplay.reddit.screens.thumb.ui.ThumbScreenFragment;

public class DefaultContainerScreenPresenter implements ContainerScreenPresenter {
    private static final String TAG = "{DefaultContainerScreenPresenter}";

    private ContainerScreenView view;
    private Layer layer;

    @Override
    public void onAttachView(@NonNull ContainerScreenView view) {
        this.view = view;
    }

    @Override
    public void onAttachView(@Nullable Bundle savedInstanceState, @NonNull ContainerScreenView view) {
        Log.d(TAG, String.format("onAttachView: savedInstanceState=[%s]; view=[%s]", savedInstanceState, view));
        this.view = view;
        if (savedInstanceState == null) {
            loadBaseLayer();
        }
    }

    private void loadBaseLayer() {
        if (layer == null) {
            setLayer(new Layer() {
                @Override
                public Fragment getContentFragment() {
                    return NewsScreenFragment.newInstance();
                }
            });
        }
        view.showLayer(layer);
    }

    private void setLayer(@NonNull Layer layer) {
        this.layer = layer;
    }

    @Override
    public void onRouteAction(Intent routeIntent) {
        if (routeIntent == null) {
            Log.e(TAG, "onRouteAction<INTENT IS NULL>");
            return;
        }
        Log.d(TAG, String.format("onRouteAction: intent=[%s]", routeIntent));
        Layer layer = resolveLayer(routeIntent);
        view.addLayer(layer);
        setLayer(layer);
    }

    private Layer resolveLayer(@NonNull Intent intent) {
        String routeAction = intent.getAction();
        final Fragment layerFragment = NewsActions.OPEN_NEWS_SCREEN_ACTION.equals(routeAction) ? NewsScreenFragment.newInstance() : ThumbScreenFragment.newInstance();
        layerFragment.setArguments(intent.getBundleExtra(NewsLocalIntent.EXTRA_BUNDLE));
        return new Layer() {
            @Override
            public Fragment getContentFragment() {
                return layerFragment;
            }
        };
    }

    @Override
    public IntentFilter getRoutesIntentFilter() {
        IntentFilter routerIntentFilter = new IntentFilter();
        routerIntentFilter.addAction(NewsActions.OPEN_NEWS_SCREEN_ACTION);
        routerIntentFilter.addAction(NewsActions.OPEN_THUMB_SCREEN_ACTION);
        return routerIntentFilter;
    }

    @Override
    public void onDetachView() {
        this.view = null;
    }
}
