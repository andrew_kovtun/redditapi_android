package com.uniplay.reddit.screens.thumb.presenter;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.domain.intent.NewsLocalIntent;
import com.uniplay.reddit.screens.thumb.ui.ThumbScreenView;

public class BaseThumbScreenPresenter implements ThumbScreenPresenter {
    private static final String TAG = "{BaseThumbScreenPresenter}";

    private ThumbScreenView view;

    private NewsEntity newsEntity;

    @Override
    public void onCreate(@Nullable Bundle arguments) {
        Log.d(TAG, String.format("onCreate: arguments=[%s]", arguments));
        if (arguments == null) {
            return;
        }
        this.newsEntity = (NewsEntity) arguments.getSerializable(NewsLocalIntent.EXTRA_ENTITY);
    }

    @Override
    public void onAttachView(@NonNull ThumbScreenView view) {
        Log.d(TAG, String.format("onAttachView: view=[%s]", view));
        this.view = view;
        view.showProgress();
        if (newsEntity == null) {
            view.hideProgress();
            return;
        }
        view.showThumb(getThumbLink(newsEntity));
        view.showTitle(newsEntity.getTitle());
    }

    private Uri getThumbLink(NewsEntity newsItem) {
        if (newsItem.getUrl() != null) {
            return Uri.parse(newsItem.getUrl());
        }
        return Uri.parse(newsEntity.getThumbnail());
    }

    @Override
    public void onLoadImageSuccess() {
        Log.d(TAG, "onLoadImageSuccess");
        if (view == null) {
            return;
        }
        view.hideProgress();
    }

    @Override
    public void onLoadImageFailure() {
        Log.e(TAG, "onLoadImageFailure");
        if (view == null) {
            return;
        }
        view.showThumb(Uri.parse(newsEntity.getThumbnail()));
    }

    @Override
    public void onDetachView() {
        Log.d(TAG, "onDetachView");
        this.view = null;
    }
}
