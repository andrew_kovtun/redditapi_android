package com.uniplay.reddit.screens.news.interactor;

import android.support.annotation.NonNull;

import com.uniplay.domain.service.LoaderCallback;
import com.uniplay.reddit.domain.entity.NewsEntity;

import java.util.List;

public interface NewsUseCase {

    void loadItems(@NonNull LoaderCallback<List<NewsEntity>> loaderCallback);

    void loadItems(@NonNull LoaderCallback<List<NewsEntity>> loaderCallback, String after);
}
