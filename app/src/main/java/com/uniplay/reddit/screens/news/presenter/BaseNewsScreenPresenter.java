package com.uniplay.reddit.screens.news.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.uniplay.domain.repository.DataStorage;
import com.uniplay.domain.service.LoaderCallback;
import com.uniplay.reddit.R;
import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.screens.news.interactor.NewsUseCase;
import com.uniplay.reddit.screens.news.ui.NewsScreenView;
import com.uniplay.reddit.screens.news.ui.NewsViewHolder;

import java.util.List;

import javax.inject.Inject;

public class BaseNewsScreenPresenter implements NewsScreenPresenter {
    private static final String TAG = "{BaseNewsScreenPresenter}";

    private final DataStorage<NewsEntity> dataStorage;
    private final ListItemsPresenter<NewsEntity, NewsViewHolder> listPresenter;
    private final NewsUseCase useCase;

    private final LoaderCallback<List<NewsEntity>> reloadCallback = new LoaderCallback<List<NewsEntity>>() {
        @Override
        public void onSuccess(List<NewsEntity> data) {
            Log.d(TAG, String.format("onSuccess: data=[%s]", data));
            listPresenter.setItems(data);
            onDataSuccessLoaded();
        }

        @Override
        public void onFailure(Throwable t) {
            Log.e(TAG, String.format("onFailure: error=[%s]", t));
            onDataFailureLoaded();
        }
    };

    private final LoaderCallback<List<NewsEntity>> loadNextCallback = new LoaderCallback<List<NewsEntity>>() {
        @Override
        public void onSuccess(List<NewsEntity> data) {
            Log.d(TAG, String.format("onSuccess: data=[%s]", data));
            listPresenter.addItems(data);
            onDataSuccessLoaded();
        }

        @Override
        public void onFailure(Throwable t) {
            Log.e(TAG, String.format("onFailure: error=[%s]", t));
            onDataFailureLoaded();
        }
    };

    private NewsScreenView view;
    private boolean isLoading;

    @Inject
    BaseNewsScreenPresenter(ListItemsPresenter listPresenter, NewsUseCase useCase, DataStorage<NewsEntity> dataStorage) {
        this.listPresenter = listPresenter;
        this.useCase = useCase;
        this.dataStorage = dataStorage;
    }

    @Override
    public void onAttachView(@NonNull NewsScreenView view) {
        Log.d(TAG, String.format("onAttachView: view=[%s]", view));
        this.view = view;
        view.showProgress();

        List<NewsEntity> cachedItems = dataStorage.get();
        if (cachedItems != null) {
            listPresenter.setItems(cachedItems);
            onDataSuccessLoaded();
        } else {
            loadItems();
        }
    }

    private void loadItems() {
        Log.d(TAG, "loadItems()");
        setLoading(true);
        useCase.loadItems(loadNextCallback);
    }

    @Override
    public void onLoadNext() {
        Log.d(TAG, "onLoadNext()");
        loadItems();
    }

    @Override
    public void onReload() {
        Log.d(TAG, "onReload()");
        dataStorage.clear();
        useCase.loadItems(reloadCallback, null);
        view.showProgress();
        setLoading(true);
        view.hideNoDataLayer();
    }

    private void onDataFailureLoaded() {
        if (listPresenter.getItemCount() < 1) {
            view.showNoDataLayer(R.string.empty_data_error);
        } else {
            view.hideNoDataLayer();
        }
        view.showError(R.string.data_load_error);
        view.hideProgress();
        setLoading(false);
    }

    private void onDataSuccessLoaded() {
        if (listPresenter.getItemCount() < 1) {
            view.showNoDataLayer(R.string.empty_data_error);
        } else {
            view.hideNoDataLayer();
        }
        view.hideProgress();
        setLoading(false);
    }

    private void setLoading(boolean isLoading) {
        this.isLoading = isLoading;
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void onDetachView() {
        Log.d(TAG, "onDetachView()");
        this.view = null;
    }
}
