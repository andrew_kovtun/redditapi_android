package com.uniplay.reddit.screens.thumb.ui;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.uniplay.common.ui.BaseFragment;
import com.uniplay.reddit.R;
import com.uniplay.reddit.application.RedditApplication;
import com.uniplay.reddit.dagger.component.DaggerThumbFragmentComponent;
import com.uniplay.reddit.dagger.component.ThumbFragmentComponent;
import com.uniplay.reddit.dagger.module.ThumbScreenModule;
import com.uniplay.reddit.screens.thumb.presenter.ThumbScreenPresenter;

import javax.inject.Inject;

public class ThumbScreenFragment extends BaseFragment<ThumbFragmentComponent> implements ThumbScreenView {
    private static final String TAG = "{ThumbScreenFragment}";

    private ImageView thumbImgView;
    private TextView titleTxtView;
    private ProgressBar progress;

    @Inject
    ThumbScreenPresenter presenter;

    public static ThumbScreenFragment newInstance() {
        return new ThumbScreenFragment();
    }

    @Override
    protected ThumbFragmentComponent buildComponent() {
        return DaggerThumbFragmentComponent.builder()
                .applicationComponent(RedditApplication.get(getContext()).getApplicationComponent())
                .thumbScreenModule(new ThumbScreenModule())
                .build();
    }

    @Override
    protected void initializeInjector(ThumbFragmentComponent component) {
        component.inject(this);
    }

    @Override
    protected Class getComponentClass() {
        return ThumbFragmentComponent.class;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, String.format("onCreate: savedInstanceState=[%s]", savedInstanceState));
        super.onCreate(savedInstanceState);
        presenter.onCreate(getArguments());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d(TAG, String.format("onCreateView: view=[%s]", ThumbScreenFragment.this));
        View view = inflater.inflate(R.layout.thumb_screen_fragment, container, false);
        this.thumbImgView = view.findViewById(R.id.thumb_container);
        this.titleTxtView = view.findViewById(R.id.title_txt);
        this.progress = view.findViewById(R.id.progress);
        presenter.onAttachView(this);
        return view;
    }

    @Override
    public void showThumb(@NonNull Uri uri) {
        Picasso.with(getContext())
                .load(uri)
                .into(thumbImgView, new Callback() {
                    @Override
                    public void onSuccess() {
                        presenter.onLoadImageSuccess();
                    }

                    @Override
                    public void onError() {
                        presenter.onLoadImageFailure();
                    }
                });
    }

    @Override
    public void showTitle(@NonNull String title) {
        titleTxtView.setText(title);
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
        presenter.onDetachView();
    }
}
