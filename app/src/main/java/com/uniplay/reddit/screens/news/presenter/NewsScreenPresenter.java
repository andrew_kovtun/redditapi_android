package com.uniplay.reddit.screens.news.presenter;

import com.uniplay.common.ui.Presenter;
import com.uniplay.reddit.screens.news.ui.NewsScreenView;

public interface NewsScreenPresenter extends Presenter<NewsScreenView> {

    void onLoadNext();

    void onReload();

    boolean isLoading();
}
