package com.uniplay.reddit.screens.news.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uniplay.reddit.R;
import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.screens.news.presenter.ListItemsPresenter;
import com.uniplay.reddit.screens.news.ui.LoadingViewHolder;
import com.uniplay.reddit.screens.news.ui.NewsViewHolder;

import javax.inject.Inject;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int CONTENT_ITEM = 0;
    public static final int LOADING_ITEM = 1;

    private ListItemsPresenter<NewsEntity, NewsViewHolder> presenter;

    @Inject
    public RecyclerAdapter(ListItemsPresenter presenter) {
        this.presenter = presenter;
        presenter.bindAdapter(this);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == LOADING_ITEM) {
            return new LoadingViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.loading_item, parent, false));
        }
        return new NewsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == LOADING_ITEM) {
            return;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onItemClick((NewsViewHolder) holder, holder.getAdapterPosition());
            }
        });
        presenter.onBindViewHolder((NewsViewHolder) holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return presenter.getItemViewType(position);
    }
}
