package com.uniplay.reddit.screens.news.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.uniplay.common.ui.BaseFragment;
import com.uniplay.common.util.PaginationScrollListener;
import com.uniplay.reddit.R;
import com.uniplay.reddit.application.RedditApplication;
import com.uniplay.reddit.dagger.component.DaggerNewsFragmentComponent;
import com.uniplay.reddit.dagger.component.NewsFragmentComponent;
import com.uniplay.reddit.dagger.module.NewsScreenModule;
import com.uniplay.reddit.domain.entity.NewsEntity;
import com.uniplay.reddit.screens.news.adapter.RecyclerAdapter;
import com.uniplay.reddit.screens.news.presenter.NewsScreenPresenter;

import javax.inject.Inject;

public class NewsScreenFragment extends BaseFragment<NewsFragmentComponent> implements NewsScreenView<NewsEntity>, SwipeRefreshLayout.OnRefreshListener {
    private static final String EXTRA_LIST_STATE = "EXTRA_LIST_STATE";
    private static final String TAG = "{NewsScreenFragment}";

    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeContainer;
    private RecyclerView recyclerView;
    private TextView emptyLayerTxtView;

    @Inject
    NewsScreenPresenter presenter;
    @Inject
    RecyclerAdapter adapter;

    public static NewsScreenFragment newInstance() {
        return new NewsScreenFragment();
    }

    @Override
    protected NewsFragmentComponent buildComponent() {
        return DaggerNewsFragmentComponent.builder()
                .applicationComponent(RedditApplication.get(getContext()).getApplicationComponent())
                .newsScreenModule(new NewsScreenModule())
                .build();
    }

    @Override
    protected void initializeInjector(NewsFragmentComponent component) {
        component.inject(this);
    }

    @Override
    protected Class getComponentClass() {
        return NewsFragmentComponent.class;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.d(TAG, String.format("onCreateView: savedInstanceState=[%s]", savedInstanceState));
        View view = inflater.inflate(R.layout.news_screen_fragment, container, false);
        swipeContainer = view.findViewById(R.id.swipe_container);
        emptyLayerTxtView = view.findViewById(R.id.empty_layer_container);
        recyclerView = view.findViewById(R.id.list_view);
        swipeContainer.setOnRefreshListener(this);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        if (savedInstanceState != null && savedInstanceState.containsKey(EXTRA_LIST_STATE)) {
            linearLayoutManager.onRestoreInstanceState(savedInstanceState.getParcelable(EXTRA_LIST_STATE));
        }
        recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadNextItems() {
                presenter.onLoadNext();
            }

            @Override
            public boolean isLoading() {
                return presenter.isLoading();
            }
        });
        presenter.onAttachView(this);
        return view;
    }

    @Override
    public void showProgress() {
        swipeContainer.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void showError(int resId) {
        Toast.makeText(getContext(), resId, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(EXTRA_LIST_STATE, linearLayoutManager.onSaveInstanceState());
        super.onSaveInstanceState(outState);
        Log.d(TAG, String.format("onSaveInstanceState: outState=[%s]", outState));
    }

    @Override
    public void showNoDataLayer(int resId) {
        Log.d(TAG, String.format("showNoDataLayer: resId=[%s]", resId));
        emptyLayerTxtView.setText(resId);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideNoDataLayer() {
        Log.d(TAG, "hideNoDataLayer()");
        emptyLayerTxtView.setText("");
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "onRefresh()");
        presenter.onReload();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView()");
        super.onDestroyView();
        presenter.onDetachView();
    }
}
