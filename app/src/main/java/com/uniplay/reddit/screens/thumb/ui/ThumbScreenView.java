package com.uniplay.reddit.screens.thumb.ui;

import android.net.Uri;
import android.support.annotation.NonNull;

public interface ThumbScreenView {

    void showThumb(@NonNull Uri uri);

    void showTitle(@NonNull String text);

    void showProgress();

    void hideProgress();
}
