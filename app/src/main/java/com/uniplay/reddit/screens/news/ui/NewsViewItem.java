package com.uniplay.reddit.screens.news.ui;

import android.net.Uri;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

public interface NewsViewItem {

    void setTitle(@NonNull String title);

    void setAuthor(@NonNull String name);

    void setDate(@NonNull String dateUserFriendly);

    void setThumb(@Nullable Uri uri);

    void setThumb(@DrawableRes int defaultDrawable);

    void setCommentNumbers(int count);

    void showToast(@StringRes int text);
}
