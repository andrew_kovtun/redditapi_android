package com.uniplay.reddit.domain.intent;

public final class NewsActions {
    public static String OPEN_THUMB_SCREEN_ACTION = "com.uniplay.reddit.domain.intent.NewsActions.OPEN_THUMB_SCREEN_ACTION";
    public static String OPEN_NEWS_SCREEN_ACTION = "com.uniplay.reddit.domain.intent.NewsActions.OPEN_NEWS_SCREEN_ACTION";
}
