package com.uniplay.reddit.domain.intent;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.uniplay.reddit.domain.entity.NewsEntity;

public class NewsLocalIntent extends Intent {
    public static final String EXTRA_ENTITY = "com.uniplay.reddit.domain.intent.NewsLocalIntent.EXTRA_ENTITY";
    public static final String EXTRA_BUNDLE = "com.uniplay.reddit.domain.intent.NewsLocalIntent.EXTRA_BUNDLE";

    public static Intent buildThumbScreenRouteIntent(@NonNull NewsEntity newsEntity) {
        Intent routeIntent = new Intent(NewsActions.OPEN_THUMB_SCREEN_ACTION);
        Bundle bundleArgs = new Bundle();
        bundleArgs.putSerializable(EXTRA_ENTITY, newsEntity);
        routeIntent.putExtra(EXTRA_BUNDLE, bundleArgs);
        return routeIntent;
    }

    public static Intent buildNewsSreenRouteIntent() {
        return new Intent(NewsActions.OPEN_NEWS_SCREEN_ACTION);
    }
}
