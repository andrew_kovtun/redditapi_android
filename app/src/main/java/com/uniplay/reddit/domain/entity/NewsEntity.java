package com.uniplay.reddit.domain.entity;

import android.support.annotation.DrawableRes;

import com.google.gson.annotations.SerializedName;
import com.uniplay.common.util.DrawableUtils;

import java.io.Serializable;
import java.util.Objects;

public class NewsEntity implements Serializable {

    private String url;

    @SerializedName("num_comments")
    private int commentNumbers;

    private String thumbnail;

    private String id;

    private String author;

    private String title;

    private String name;

    private int defaultDrawable;

    @SerializedName("subreddit")
    private String type;

    private long created_utc;

    public NewsEntity() {
    }

    public NewsEntity(String url, int commentNumbers, String thumbnail, String id, String author, String title, String name) {
        this.url = url;
        this.commentNumbers = commentNumbers;
        this.thumbnail = thumbnail;
        this.id = id;
        this.author = author;
        this.title = title;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public int getCommentNumbers() {
        return commentNumbers;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public long getCreatedInSec() {
        return created_utc;
    }

    @DrawableRes
    public int getDefaultDrawable() {
        if (defaultDrawable == 0) {
            defaultDrawable = DrawableUtils.getInstance().getRandDrawable();
        }
        return defaultDrawable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewsEntity that = (NewsEntity) o;
        return commentNumbers == that.commentNumbers &&
                created_utc == that.created_utc &&
                Objects.equals(url, that.url) &&
                Objects.equals(thumbnail, that.thumbnail) &&
                Objects.equals(id, that.id) &&
                Objects.equals(author, that.author) &&
                Objects.equals(title, that.title) &&
                Objects.equals(name, that.name) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {

        return Objects.hash(url, commentNumbers, thumbnail, id, author, title, name, type, created_utc);
    }
}

