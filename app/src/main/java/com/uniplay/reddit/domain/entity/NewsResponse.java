package com.uniplay.reddit.domain.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NewsResponse {
    private Data data;

    private class Data {
        private int dist;
        private List<ChildrenEntity> children;
        private String after;
        private String before;
    }

    private class ChildrenEntity {
        @SerializedName("data")
        private NewsEntity items;
    }

    public List<NewsEntity> getNewsEntities() {
        List<NewsEntity> composedEntities = new ArrayList<>();
        for (ChildrenEntity childrenEntity : data.children) {
            composedEntities.add(childrenEntity.items);
        }
        return composedEntities;
    }

    public String getAfter() {
        return data.after;
    }

    public String getBefore() {
        return data.after;
    }

    public String getDist() {
        return data.after;
    }
}
