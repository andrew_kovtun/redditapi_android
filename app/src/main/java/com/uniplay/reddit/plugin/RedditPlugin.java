package com.uniplay.reddit.plugin;

import com.uniplay.domain.plugin.Plugin;

public class RedditPlugin implements Plugin {
    @Override
    public String getName() {
        return "RedditPlugin";
    }

    @Override
    public String getBaseUrl() {
        return "https://www.reddit.com/";
    }

    @Override
    public boolean isEnable() {
        return true;
    }
}
