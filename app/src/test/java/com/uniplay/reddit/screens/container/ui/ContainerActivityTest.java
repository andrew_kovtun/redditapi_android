package com.uniplay.reddit.screens.container.ui;

import com.uniplay.reddit.screens.container.presenter.ContainerScreenPresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;

import static org.mockito.Mockito.verify;

@RunWith(RobolectricTestRunner.class)
public class ContainerActivityTest {
    private ContainerActivity containerActivity;

    @Mock
    ContainerScreenPresenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ActivityController<ContainerActivity> containerActivityActivityController = Robolectric.buildActivity(ContainerActivity.class);
        containerActivityActivityController.create();
        containerActivity = containerActivityActivityController.get();
        containerActivity.presenter = presenter;
    }

    @Test
    public void testOnDestroyExpectPresenterCall() {
        containerActivity.onDestroy();
        verify(presenter).onDetachView();
    }
}