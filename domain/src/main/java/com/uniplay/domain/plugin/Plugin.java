package com.uniplay.domain.plugin;

public interface Plugin {
    String getName();

    String getBaseUrl();

    boolean isEnable();
}
