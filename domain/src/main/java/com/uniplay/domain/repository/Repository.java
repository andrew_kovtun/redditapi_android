package com.uniplay.domain.repository;

import com.uniplay.domain.model.DomainType;

public interface Repository<T> {

    void add(DomainType domainType, T item);

    void remove(DomainType domainType);
}
