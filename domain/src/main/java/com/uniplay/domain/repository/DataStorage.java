package com.uniplay.domain.repository;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DataStorage<T> {
    private List<T> dataStorage;

    public List<T> get() {
        return dataStorage;
    }

    public void put(List<T> data) {
        if (dataStorage == null) {
            dataStorage = Collections.synchronizedList(new LinkedList<T>());
        }
        dataStorage.addAll(data);
    }

    public void clear() {
        if (dataStorage != null) {
            dataStorage.clear();
        }
    }
}
