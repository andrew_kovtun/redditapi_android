package com.uniplay.domain.service;

public interface LoaderCallback<T> {

    void onSuccess(T data);

    void onFailure(Throwable t);
}
