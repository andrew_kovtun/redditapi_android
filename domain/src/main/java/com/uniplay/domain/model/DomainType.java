package com.uniplay.domain.model;

import java.io.Serializable;

public interface DomainType extends Serializable {
    String getType();
}
