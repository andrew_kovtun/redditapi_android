package com.uniplay.domain.repository;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.fest.assertions.api.Assertions.assertThat;

public class DataStorageTest {
    private Object TEST_OBJ_1 = new Object();
    private Object TEST_OBJ_2 = new Object();

    @Test
    public void testNoPutThenGetNull() {
        DataStorage dataStorage = new DataStorage();
        assertThat(dataStorage.get()).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testPutNullThenGetException() {
        DataStorage dataStorage = new DataStorage();
        dataStorage.put(null);
    }

    @Test
    public void testPutOneItemThanGetOneItem() {
        DataStorage<Object> dataStorage = new DataStorage<>();
        dataStorage.put(Collections.singletonList(TEST_OBJ_1));
        assertThat(dataStorage.get()).isNotNull();
        assertThat(dataStorage.get().size()).isEqualTo(1);
    }

    @Test
    public void testPutTwoItemsThanGetTwoItems() {
        DataStorage<Object> dataStorage = new DataStorage<>();
        dataStorage.put(Arrays.asList(TEST_OBJ_1, TEST_OBJ_2));
        assertThat(dataStorage.get()).isNotNull();
        assertThat(dataStorage.get().size()).isEqualTo(2);
    }

    @Test
    public void testAddOneAndClearThanReturnSizeZero() {
        DataStorage<Object> dataStorage = new DataStorage<>();
        dataStorage.put(Arrays.asList(TEST_OBJ_1, TEST_OBJ_2));
        assertThat(dataStorage.get()).isNotNull();
        assertThat(dataStorage.get().size()).isEqualTo(2);
        dataStorage.clear();
        assertThat(dataStorage.get()).isNotNull();
        assertThat(dataStorage.get().size()).isEqualTo(0);
    }

    @Test
    public void testEmptyListClearThanReturnNull() {
        DataStorage<Object> dataStorage = new DataStorage<>();
        dataStorage.clear();
        assertThat(dataStorage.get()).isNull();
    }
}