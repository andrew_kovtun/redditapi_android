package com.uniplay.common.persister;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ComponentPersisterTest {
    private final Object TEST_OBJ_1 = new Object();
    private final Object TEST_OBJ_2 = new Object();

    private final ComponentPersister componentPersister = ComponentPersister.getInstance();

    @Test
    public void testPersistNullThanRestoreNull() {
        componentPersister.persistComponent(null, null);
        Object o = componentPersister.restoreComponent(null);
        assertNull(o);
    }

    @Test
    public void testPersistObj1ThanRestoreObj1() {
        componentPersister.persistComponent(TEST_OBJ_1.getClass(), TEST_OBJ_1);
        Object o = componentPersister.restoreComponent(TEST_OBJ_1.getClass());
        assertEquals(TEST_OBJ_1, o);
    }

    @Test
    public void testPersistObj1ThanPersistObj2RestoreObj2() {
        componentPersister.persistComponent(TEST_OBJ_1.getClass(), TEST_OBJ_1);
        componentPersister.persistComponent(TEST_OBJ_2.getClass(), TEST_OBJ_2);
        Object o = componentPersister.restoreComponent(TEST_OBJ_2.getClass());
        assertEquals(TEST_OBJ_2, o);
    }

}