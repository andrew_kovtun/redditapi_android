package com.uniplay.common.ui;

import com.uniplay.common.dagger.component.DaggerTestFragmentComponent;
import com.uniplay.common.dagger.component.TestFragmentComponent;
import com.uniplay.common.dagger.module.TestFragmentModule;
import com.uniplay.common.persister.ComponentPersister;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
public class BaseActivityTest {

    @Mock
    private ComponentPersister componentPersister;

    private TestFragmentComponent TEST_COMPONENT_1 = DaggerTestFragmentComponent.builder()
            .testFragmentModule(new TestFragmentModule())
            .build();

    private TestFragmentComponent TEST_COMPONENT_2 = DaggerTestFragmentComponent.builder()
            .testFragmentModule(new TestFragmentModule())
            .build();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(componentPersister.restoreComponent(TestFragmentComponent.class)).thenReturn(TEST_COMPONENT_2);
    }

    @Test
    public void testComponent1NotEqualComponent2() {
        assertNotEquals(TEST_COMPONENT_1, TEST_COMPONENT_2);
    }

    @Test
    public void testOnCreateWithEmptyPersisterThanBuildNewComponent() {
        ActivityController<DefaultTestActivity> testActivityController = Robolectric.buildActivity(DefaultTestActivity.class);
        testActivityController.get().setComponent(TEST_COMPONENT_1);
        testActivityController.get().setPersister(ComponentPersister.getInstance());
        testActivityController.create();
        assertEquals(testActivityController.get().getComponent(), TEST_COMPONENT_1);
    }

    @Test
    public void testOnCreateWithBundleThanRestoreComponent() {
        ActivityController<DefaultTestActivity> testActivityController = Robolectric.buildActivity(DefaultTestActivity.class);
        testActivityController.get().setPersister(componentPersister);
        testActivityController.get().setComponent(TEST_COMPONENT_1);
        testActivityController.create();
        assertEquals(testActivityController.get().getComponent(), TEST_COMPONENT_2);
    }
}