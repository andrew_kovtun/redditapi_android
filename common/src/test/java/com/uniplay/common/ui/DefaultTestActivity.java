package com.uniplay.common.ui;

import com.uniplay.common.dagger.component.TestFragmentComponent;
import com.uniplay.common.persister.ComponentPersister;

public class DefaultTestActivity extends BaseActivity<TestFragmentComponent> {

    private TestFragmentComponent component;
    private ComponentPersister persister;

    @Override
    protected TestFragmentComponent buildComponent() {
        return component;
    }

    public void setComponent(TestFragmentComponent component) {
        this.component = component;
    }

    public TestFragmentComponent getComponent() {
        return component;
    }

    @Override
    protected void initializeInjector(TestFragmentComponent component) {
        component.inject(this);
        setComponent(component);
    }

    @Override
    protected Class getComponentClass() {
        return TestFragmentComponent.class;
    }

    @Override
    protected ComponentPersister getPersister() {
        return persister;
    }

    protected void setPersister(ComponentPersister componentPersister) {
        this.persister = componentPersister;
    }
}
