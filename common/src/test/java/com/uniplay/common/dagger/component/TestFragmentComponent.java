package com.uniplay.common.dagger.component;

import com.uniplay.common.dagger.module.TestFragmentModule;
import com.uniplay.common.ui.DefaultTestActivity;

import dagger.Component;

@Component(modules = {TestFragmentModule.class})
public interface TestFragmentComponent {

    void inject(DefaultTestActivity testBaseActivity);
}
