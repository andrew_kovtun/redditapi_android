package com.uniplay.common.dagger.module;

import com.uniplay.common.util.DrawableUtils;

import dagger.Module;
import dagger.Provides;

@Module
public class TestFragmentModule {

    @Provides
    DrawableUtils provideDrawableUtils() {
        return DrawableUtils.getInstance();
    }
}
