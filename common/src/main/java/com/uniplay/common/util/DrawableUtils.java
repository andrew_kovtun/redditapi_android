package com.uniplay.common.util;

import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;

import com.uniplay.common.R;

import java.util.Random;

public final class DrawableUtils {

    private static DrawableUtils instance;

    private final int[] colors = new int[]{
            R.color.blue_material_color,
            R.color.green_material_color,
            R.color.pink_material_color,
            R.color.yellow_material_color};

    private final int[] drawables = new int[]{
            R.drawable.blue_circle_drawable,
            R.drawable.green_circle_drawable,
            R.drawable.pink_circle_drawable,
            R.drawable.yellow_circle_drawable};

    private final Random r = new Random();

    private DrawableUtils() {
    }

    public static DrawableUtils getInstance() {
        if (instance == null) {
            instance = new DrawableUtils();
        }
        return instance;
    }

    public int getRandColor() {
        return colors[r.nextInt(colors.length)];
    }

    public int getRandDrawable() {
        return drawables[r.nextInt(drawables.length)];
    }

    public static ShapeDrawable createCircle(int width, int height, int color) {
        ShapeDrawable oval = new ShapeDrawable(new OvalShape());
        oval.setIntrinsicHeight(height);
        oval.setIntrinsicWidth(width);
        oval.getPaint().setColor(color);
        return oval;
    }
}
