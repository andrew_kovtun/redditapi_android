package com.uniplay.common.ui;

import android.support.v4.app.Fragment;

public interface Layer {
    Fragment getContentFragment();
}
