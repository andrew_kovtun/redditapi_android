package com.uniplay.common.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.uniplay.common.persister.ComponentPersister;

public abstract class BaseActivity<T> extends AppCompatActivity {
    private ComponentPersister persister = ComponentPersister.getInstance();
    private T daggerComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getPersister().restoreComponent(getComponentClass()) != null) {
            daggerComponent = (T) getPersister().restoreComponent(getComponentClass());
        }
        if (daggerComponent == null) {
            daggerComponent = buildComponent();
        }
        initializeInjector(daggerComponent);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        persister.persistComponent(getComponentClass(), daggerComponent);
        super.onSaveInstanceState(outState);
    }

    protected ComponentPersister getPersister() {
        return persister;
    }

    protected abstract T buildComponent();

    protected abstract void initializeInjector(T component);

    protected abstract Class getComponentClass();
}
