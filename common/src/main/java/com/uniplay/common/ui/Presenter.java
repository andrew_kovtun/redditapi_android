package com.uniplay.common.ui;

import android.support.annotation.NonNull;

public interface Presenter<T> {

    void onAttachView(@NonNull T view);

    void onDetachView();
}
