package com.uniplay.common.lifecycle;

public interface LifecycleBehaviour {
    void onCreate();

    void onStop();

    void onStart();

    void onDestroy();
}
