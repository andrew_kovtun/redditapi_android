package com.uniplay.common.persister;

import java.util.HashMap;
import java.util.Map;

public class ComponentPersister {
    private static ComponentPersister instance;

    private ComponentPersister() {
    }

    public static ComponentPersister getInstance() {
        if (instance == null) {
            instance = new ComponentPersister();
        }
        return instance;
    }

    private Map<Class, Object> persisterMap = new HashMap<>();

    public void persistComponent(Class clazz, Object component) {
        persisterMap.put(clazz, component);
    }

    public Object restoreComponent(Class clazz) {
        return persisterMap.get(clazz);
    }
}
